#!/bin/bash
build_number = 21
sudo docker ps 
echo "Loggin in to docker"
aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin 004126885931.dkr.ecr.ap-south-1.amazonaws.com

echo "Stop current Container"
sudo docker stop milan-react-app
echo "Remove Old container"
sudo docker rm milan-react-app

echo "Fetching the latest image"
sudo docker pull 004126885931.dkr.ecr.ap-south-1.amazonaws.com/reactjs_repo:$build_number
echo "Starting the new Container"
sudo docker run --name milan-react-app -d -p 80:80 004126885931.dkr.ecr.ap-south-1.amazonaws.com/reactjs_repo:$build_number